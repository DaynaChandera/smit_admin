# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.shortcuts import resolve_url
from jsonfield import JSONField
from django.contrib.auth.models import User



ROLE_CHOiCE = (
    ('Admin', 'Admin'),
    ('SubAdmin', 'SubAdmin')
)
class Admin(User):
    admin_id = models.AutoField(db_column='Admin_Id', primary_key=True)  # Field name made lowercase.
    admin_name = models.CharField(db_column='Admin_Name', max_length=255)  # Field name made lowercase.
    admin_roles = models.CharField(db_column='Admin_Roles', choices=ROLE_CHOiCE, max_length=8)  # Field name made lowercase.
    admin_password = models.CharField(db_column='Admin_Password', max_length=255)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='IsActive')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'admin'

    USERNAME_FIELD = 'admin_name'
    REQUIRED_FIELDS = ['admin_roles', 'isactive']


class Auth(models.Model):
    auth_id = models.AutoField(db_column='Auth_Id', primary_key=True)  # Field name made lowercase.
    token = models.CharField(db_column='Token', max_length=255)  # Field name made lowercase.
    currently_playing = models.IntegerField(db_column='Currently_Playing')  # Field name made lowercase.
    device_id = models.CharField(db_column='Device_Id', max_length=255, blank=True, null=True)  # Field name made lowercase.
    useruserid = models.OneToOneField('User', models.DO_NOTHING, db_column='userUserId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'auth'


class Buddies(models.Model):
    buddies_id = models.AutoField(db_column='Buddies_Id', primary_key=True)  # Field name made lowercase.
    req_status = models.CharField(db_column='Req_status', max_length=8)  # Field name made lowercase.
    requestbyuserid = models.ForeignKey('User', models.DO_NOTHING, db_column='requestByUserId', blank=True, null=True, related_name='req')  # Field name made lowercase.
    requesttouserid = models.ForeignKey('User', models.DO_NOTHING, db_column='requestToUserId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'buddies'


class Gamedata(models.Model):
    gamedata_id = models.AutoField(db_column='GameData_Id', primary_key=True)  # Field name made lowercase.
    level = models.IntegerField(db_column='Level')  # Field name made lowercase.
    total_wins = models.IntegerField(db_column='Total_Wins')  # Field name made lowercase.
    total_loss = models.IntegerField(db_column='Total_Loss')  # Field name made lowercase.
    xp_point = models.IntegerField(db_column='Xp_point')  # Field name made lowercase.
    token_captured = models.IntegerField(db_column='Token_Captured')  # Field name made lowercase.
    opp_token_captured = models.IntegerField(db_column='Opp_Token_Captured')  # Field name made lowercase.
    useruserid = models.OneToOneField('User', models.DO_NOTHING, db_column='userUserId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'gamedata'


class Lobby(models.Model):
    lobby_id = models.AutoField(db_column='Lobby_Id', primary_key=True)  # Field name made lowercase.
    lobby_name = models.CharField(db_column='Lobby_Name', max_length=255)  # Field name made lowercase.
    lobby_type = models.CharField(db_column='Lobby_Type', max_length=255)  # Field name made lowercase.
    lobby_total_amount = models.IntegerField(db_column='Lobby_Total_Amount')  # Field name made lowercase.
    lobby_buy_in = models.IntegerField(db_column='Lobby_Buy_In')  # Field name made lowercase.
    room_id = models.CharField(db_column='Room_Id', max_length=255)  # Field name made lowercase.
    lobby_status = models.CharField(db_column='Lobby_Status', max_length=8)  # Field name made lowercase.
    recorded_move = JSONField(db_column='Recorded_Move', blank=True, null=True)  # Field name made lowercase.
    variationvariationid = models.ForeignKey('Variation', models.DO_NOTHING, db_column='variationVariationId', blank=True, null=True)  # Field name made lowercase.
    lobbyowneruserid = models.ForeignKey('User', models.DO_NOTHING, db_column='lobbyOwnerUserId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'lobby'


class LobbyHistory(models.Model):
    lobby_history_id = models.AutoField(db_column='Lobby_History_Id', primary_key=True)  # Field name made lowercase.
    winning_status = models.CharField(db_column='Winning_Status', max_length=4)  # Field name made lowercase.
    winning_price = models.IntegerField(db_column='Winning_Price')  # Field name made lowercase.
    lobbylobbyid = models.ForeignKey(Lobby, models.DO_NOTHING, db_column='lobbyLobbyId', blank=True, null=True)  # Field name made lowercase.
    useruserid = models.ForeignKey('User', models.DO_NOTHING, db_column='userUserId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'lobby_history'


class User(models.Model):
    user_id = models.CharField(db_column='User_Id', primary_key=True, max_length=255)  # Field name made lowercase.
    user_name = models.CharField(db_column='User_Name', unique=True, max_length=255)  # Field name made lowercase.
    user_displayname = models.CharField(db_column='User_DisplayName', max_length=255)  # Field name made lowercase.
    user_password = models.CharField(db_column='User_Password', max_length=255)  # Field name made lowercase.
    user_country = models.CharField(db_column='User_Country', max_length=255)  # Field name made lowercase.
    user_mobile_number = models.BigIntegerField(db_column='User_Mobile_Number', unique=True, blank=True, null=True)  # Field name made lowercase.
    user_email_id = models.CharField(db_column='User_Email_Id', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    user_image = models.TextField(db_column='User_Image')  # Field name made lowercase.
    user_level = models.IntegerField(db_column='User_Level')  # Field name made lowercase.
    reference_id = models.CharField(db_column='Reference_Id', max_length=255)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='IsActive')  # Field name made lowercase.
    referencebyuserid = models.ForeignKey('self', models.DO_NOTHING, db_column='referenceByUserId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'user'


class Variation(models.Model):
    variation_id = models.AutoField(db_column='Variation_Id', primary_key=True)  # Field name made lowercase.
    variation_name = models.CharField(db_column='Variation_Name', max_length=255)  # Field name made lowercase.
    variation_initial = models.CharField(db_column='Variation_Initial', max_length=3)  # Field name made lowercase.
    player_count = models.IntegerField(db_column='Player_Count')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'variation'


class Wallet(models.Model):
    wallet_id = models.AutoField(db_column='Wallet_Id', primary_key=True)  # Field name made lowercase.
    user_chips = models.FloatField(db_column='User_Chips')  # Field name made lowercase.
    user_bonus_cash = models.IntegerField(db_column='User_Bonus_Cash')  # Field name made lowercase.
    user_win_amount = models.FloatField(db_column='User_Win_Amount')  # Field name made lowercase.
    user_loss_amount = models.FloatField(db_column='User_Loss_Amount')  # Field name made lowercase.
    gems = models.IntegerField(db_column='Gems')  # Field name made lowercase.
    useruserid = models.OneToOneField(User, models.DO_NOTHING, db_column='userUserId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'wallet'


